# Balance Manager

Balance Manager is an Android application that helps you balance your economy.

# Requirements

* Android Studio
* An Android SDK version installed, at least version 14.

# Building

Open the project in Android Studio, let it start Gradle and after a little while you should be able to launch the application from the 'Run' menu.
