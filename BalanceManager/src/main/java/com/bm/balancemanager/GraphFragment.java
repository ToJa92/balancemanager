package com.bm.balancemanager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.achartengine.ChartFactory;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYValueSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

/**
 * Created by erik on 2013-11-28.
 */
public class GraphFragment extends BaseFragment {
    private static final String TITLE = "Graph";

    public static GraphFragment newInstance() {
        GraphFragment fragment = new GraphFragment();
        Bundle args = new Bundle();
        args.putString("title", TITLE);
        fragment.setArguments(args);
        return fragment;
    }

    public GraphFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_graph, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        //check if we need to get region data.
        //if we have neither region data, nor a running retrieveregionmodel
        //execute retrieve region model
                /*if (regionData == null) {
                        //execute retrieveregionmodel unless not executing already
                        if (retrieveRegionTask == null) {
                                retrieveRegionTask = new RetrieveRegionTask();
                                retrieveRegionTask.execute(new HashMap<?,?>[]{connectionData});
                        }
                }*/

        drawChart();
    }

    private void drawChart() {
        XYMultipleSeriesDataset series = new XYMultipleSeriesDataset();
        XYValueSeries newTicketSeries = new XYValueSeries("New Tickets");
        newTicketSeries.add(1, 2, 14);
        newTicketSeries.add(2, 2, 12);
        newTicketSeries.add(3, 2, 18);
        newTicketSeries.add(4, 2, 5);
        newTicketSeries.add(5, 2, 1);
        series.addSeries(newTicketSeries);
        XYValueSeries fixedTicketSeries = new XYValueSeries("Fixed Tickets");
        fixedTicketSeries.add(1, 1, 7);
        fixedTicketSeries.add(2, 1, 4);
        fixedTicketSeries.add(3, 1, 18);
        fixedTicketSeries.add(4, 1, 3);
        fixedTicketSeries.add(5, 1, 1);
        series.addSeries(fixedTicketSeries);

        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        renderer.setAxisTitleTextSize(16);
        renderer.setChartTitleTextSize(20);
        renderer.setLabelsTextSize(15);
        renderer.setRange(new double[]{0, 6, 0, 6});

        //renderer.setMargins(new int[] { 20, 30, 15, 0 });
        XYSeriesRenderer newTicketRenderer = new XYSeriesRenderer();
        newTicketRenderer.setColor(Color.BLUE);
        renderer.addSeriesRenderer(newTicketRenderer);
        XYSeriesRenderer fixedTicketRenderer = new XYSeriesRenderer();
        fixedTicketRenderer.setColor(Color.GREEN);
        renderer.addSeriesRenderer(fixedTicketRenderer);

        renderer.setXLabels(0);
        renderer.setYLabels(0);
        renderer.setDisplayChartValues(false);
        renderer.setShowGrid(false);
        renderer.setShowLegend(false);
        renderer.setShowLabels(true);

        //renderer.setZoomEnabled(false, false);

        GraphicalView chartView;
                /*if (chartView != null) {
                        chartView.repaint();
                }
                else { */
        chartView = ChartFactory.getBubbleChartView(getActivity(), series, renderer);
        //}


        LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.new_series);
        //layout.removeAllViews();
        layout.addView(chartView, new LayoutParams(960,
                LayoutParams.WRAP_CONTENT));
    }

}
