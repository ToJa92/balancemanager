package com.bm.balancemanager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vessel.VesselSDK;
import com.vessel.VesselAB;
import com.vessel.enums.VesselEnums;
import com.vessel.enums.VesselEnums.TestVariation;
import com.vessel.interfaces.ABListener;

import java.util.ArrayList;


public class MainActivity extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    private SharedPreferences preference_h = null;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    protected AlertDialog.Builder catBuilder, accCreatorAlert;
    protected DataBaseHandler db_h = null;
    private int previousItem = 0;
    private int currentItem = 0;

    protected void displayNewAccCreator() {
        accCreatorAlert = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        accCreatorAlert.setView(inflater.inflate(R.layout.acc_creator_alert, null))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog thisDialog = (Dialog) dialog;
                        EditText accName = (EditText) thisDialog.findViewById(R.id.add_acc_name);
                        EditText accBalance = (EditText) thisDialog.findViewById(R.id.add_acc_balance);
                        db_h.createAccount(accName.getText().toString(), Float.parseFloat(accBalance.getText().toString()));
                        Toast.makeText(getApplicationContext(),
                                "Added " + accName.getText().toString(), Toast.LENGTH_SHORT).show();
                        preference_h.edit().putBoolean("SETUP_DONE", true).commit();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        accCreatorAlert.setTitle(R.string.set_accountCreate);
        accCreatorAlert.create();
        accCreatorAlert.show();
    }

    protected void displayNewCatCreator(){
        catBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        catBuilder.setView(inflater.inflate(R.layout.cat_creator_alert, null))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog thisDialog = (Dialog) dialog;
                        EditText catName = (EditText) thisDialog.findViewById(R.id.addCat_desc);
                        db_h.createCat(catName.getText().toString());
                        Toast.makeText(getApplicationContext(),
                                "Added " + catName.getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //LoginDialogFragment.this.getDialog().cancel();
                        dialog.dismiss();
                    }
                });
        catBuilder.setTitle(R.string.set_categoryCreate);
        catBuilder.create();
        catBuilder.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // When setContentView is called, onNavigationDrawerItemSelected() is triggered.
        // So we must get the preference handle first.
        preference_h = PreferenceManager.getDefaultSharedPreferences(this);
        db_h = new DataBaseHandler(getApplicationContext());

        VesselSDK.initialize(getApplicationContext(), "TklHQzRxZVo1VjdoODFsUE9SSkFLMFVY");

        setContentView(R.layout.activity_main);

        VesselAB.reloadTest();

        if (savedInstanceState != null)
        {
            return;
        }
        else
        {

        }

        VesselAB.setABListener(new ABListener() {
            @Override
            public void testLoading() {

            }

            @Override
            public void testLoaded(String s, TestVariation testVariation) {

            }
            @Override
            public void testNotAvailable(TestVariation testVariation) {
                BaseFragment newF;
                if(VesselAB.whichVariation() == TestVariation.A){
                    // Load new fragment, else fail over to old fragment
                    newF = new newSettingsFragment();
                }else{
                    newF = new SettingsFragment();
                }
                getFragmentManager().beginTransaction().add(R.id.container, newF).commit();
                Toast.makeText(getApplicationContext(), "Currently we have variation "+ VesselAB.whichVariation(), Toast.LENGTH_LONG).show();
            }
        });

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));


    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        currentItem = position;
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        // Show user the initial setup if not already done
        if(! preference_h.getBoolean("SETUP_DONE", false)) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, InitialSetup.newInstance())
                    .commit();
            return;
        }

        Log.w("MA", "trolo: " + position);

        Fragment mainView = null;

        if(position < 5)
            previousItem = position;
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        switch(position) {
            case 0:
                mainView = MainFragment.newInstance();
                break;
            case 1:
                mainView = AddPurchase.newInstance();
                break;
            case 2:
                mainView = AddIncome.newInstance();
                break;
            case 3:
                mainView = HistoryFragment.newInstance();
                break;
            case 4:
                mainView = SettingsFragment.newInstance();
                break;
            case 5:
                displayNewAccCreator();
                mNavigationDrawerFragment.selectItem(previousItem);
                return;
            case 6:
                displayNewCatCreator();
                mNavigationDrawerFragment.selectItem(previousItem);
                return;
            case 7:
                mainView = GraphFragment.newInstance();
                break;
            default:
                mainView = new Fragment();
                previousItem = 0;
                break;
        }

        fragmentManager.beginTransaction()
                .replace(R.id.container, mainView)
                .addToBackStack(null)
                .commit();
    }

    public void onSectionAttached(String title) {
        mTitle = title;
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add) {
            onNavigationDrawerItemSelected(1);
            getActionBar().setTitle("Add purchase");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class MainFragment extends BaseFragment {

        private static final String TITLE = "Home";

        public static MainFragment newInstance() {
            MainFragment fragment = new MainFragment();
            Bundle args = new Bundle();
            args.putString("title", TITLE);
            fragment.setArguments(args);
            return fragment;
        }

        public MainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            ArrayList<accountListObject> accounts;
            accountListAdapter aListAdapter = new accountListAdapter(getActivity());
            accounts = db_h.getMainAccounts();
            aListAdapter.addAll(accounts);

            ListView actList = (ListView) rootView.findViewById(R.id.accountList);
            actList.setAdapter(aListAdapter);

            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getString("title"));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(currentItem == 0)
        {
            System.exit(0);
        } else {
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .addToBackStack(null)
                    .commit();
            mNavigationDrawerFragment.selectItem(0);
            currentItem = 0;
            getActionBar().setTitle("Home");
        }
    }

    @Override
    protected void onPause() {
        VesselAB.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        VesselAB.onResume();
        super.onResume();
    }
}
