package com.bm.balancemanager;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class AddPurchase extends BaseFragment {
    private static final String TITLE = "Add purchase";
    private static Integer selectedCat = 1;
    private static Integer selectedAcc = 1;

    public static AddPurchase newInstance() {
        AddPurchase fragment = new AddPurchase();
        Bundle args = new Bundle();
        args.putString("title", TITLE);
        fragment.setArguments(args);
        return fragment;
    }

    public AddPurchase() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_purchase, container, false);

        Button  exec = (Button) rootView.findViewById(R.id.exec);
        exec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText pur_desc = (EditText) rootView.findViewById(R.id.pur_desc);
                final EditText pur_amount = (EditText) rootView.findViewById(R.id.pur_amount);

                float amount;
                try {
                    amount = Float.parseFloat(pur_amount.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(getActivity(), "You must enter a valid amount!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(isEmpty(pur_desc) || isEmpty(pur_amount))
                {
                    Toast.makeText(getActivity(), "All fields are required", Toast.LENGTH_SHORT).show();
                    return;
                }

                db_h.makeTransaction(pur_desc.getText().toString(), -amount, selectedCat, selectedAcc);

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        Spinner catSpinner = (Spinner) rootView.findViewById(R.id.catSpinner);
        ArrayList<String> cats = db_h.getCats();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, cats);
        catSpinner.setAdapter(adapter);

        catSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                //pos starts on 0
                selectedCat = position+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        Spinner accSpinner = (Spinner) rootView.findViewById(R.id.accSpinner);
        ArrayList<String> accs = db_h.getAccounts();
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, accs);
        accSpinner.setAdapter(adapter2);

        accSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //pos starts on 0
                selectedAcc = position + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        return rootView;
    }
}
