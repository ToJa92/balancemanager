package com.bm.balancemanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class accountListAdapter extends ArrayAdapter<accountListObject> {
    // View lookup cache
    private static class ViewHolder
    {
        TextView name;
        TextView sum;
    }

    public accountListAdapter(Context context)
    {
        super(context, R.layout.account_list_item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // Get the data item for this position
        accountListObject content = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.account_list_item, null);
            try {
                viewHolder.name = (TextView) convertView.findViewById(R.id.name);
                viewHolder.sum = (TextView) convertView.findViewById(R.id.sum);
                convertView.setTag(viewHolder);
            } catch (java.lang.NullPointerException e) {
                e.printStackTrace();
            }
        } else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object
        viewHolder.name.setText(content.name);
        viewHolder.sum.setText(content.sum);

        // Return the completed view to render on screen
        return convertView;
    }
}