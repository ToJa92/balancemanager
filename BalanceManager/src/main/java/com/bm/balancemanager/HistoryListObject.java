package com.bm.balancemanager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistoryListObject {
    public HistoryListObject(){
        this.description = "ERROR";
        this.data = "ERROR";
        this.time = 0l;
        this.category = "ERROR";
    }

    public HistoryListObject(JSONObject object){
        try {
            this.description = object.getString("description");
            this.data = object.getString("data");
            this.time = object.getLong("time");
            this.category = object.getString("category");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<HistoryListObject> fromJson(JSONArray jsonObjects) {
        ArrayList<HistoryListObject> listobj = new ArrayList<HistoryListObject>();
        for(int i = 0; i < jsonObjects.length(); ++i){
            try {
                listobj.add(new HistoryListObject(jsonObjects.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return listobj;
    }
    public String description;
    public String data;
    public Long time;
    public String category;
}
