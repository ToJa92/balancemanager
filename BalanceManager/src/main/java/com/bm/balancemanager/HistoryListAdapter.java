package com.bm.balancemanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;

public class HistoryListAdapter extends ArrayAdapter<HistoryListObject>
{
    // View lookup cache
    private static class ViewHolder
    {
        TextView description;
        TextView data;
        TextView time;
        TextView category;
    }

    public HistoryListAdapter(Context context)
    {
        super(context, R.layout.history_list_item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // Get the data item for this position
        HistoryListObject content = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.history_list_item, null);
            try {
                viewHolder.description = (TextView) convertView.findViewById(R.id.description);
                viewHolder.data = (TextView) convertView.findViewById(R.id.data);
                viewHolder.time = (TextView) convertView.findViewById(R.id.time);
                viewHolder.category = (TextView) convertView.findViewById(R.id.category);
                convertView.setTag(viewHolder);
            } catch (java.lang.NullPointerException e) {
                e.printStackTrace();
            }
        } else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object
        viewHolder.description.setText(content.description);
        viewHolder.data.setText(content.data);
        viewHolder.category.setText(content.category);

        String time = new SimpleDateFormat("kk:mm dd-MM-yy").format(content.time * 1000L);
        viewHolder.time.setText(time);

        // Return the completed view to render on screen
        return convertView;
    }
}