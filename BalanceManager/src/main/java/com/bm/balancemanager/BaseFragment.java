package com.bm.balancemanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class BaseFragment extends Fragment{
    protected AlertDialog.Builder catBuilder, accCreatorAlert;
    protected DataBaseHandler db_h = null;
    protected SharedPreferences preference_h = null;
    FragmentManager fragment_h = null;
    protected View rootView = null;

    protected boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        db_h = new DataBaseHandler(getActivity());
        preference_h = PreferenceManager.getDefaultSharedPreferences(getActivity());
        fragment_h = getFragmentManager();
        ((MainActivity) activity).onSectionAttached(
                getArguments().getString("title"));
    }

    protected void displayNewAccCreator() {
        accCreatorAlert = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        accCreatorAlert.setView(inflater.inflate(R.layout.acc_creator_alert, null))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog thisDialog = (Dialog) dialog;
                        EditText accName = (EditText) thisDialog.findViewById(R.id.add_acc_name);
                        EditText accBalance = (EditText) thisDialog.findViewById(R.id.add_acc_balance);
                        db_h.createAccount(accName.getText().toString(), Float.parseFloat(accBalance.getText().toString()));
                        Toast.makeText(getActivity(),
                                "Added " + accName.getText().toString(), Toast.LENGTH_SHORT).show();
                        preference_h.edit().putBoolean("SETUP_DONE", true).commit();
                        fragment_h.beginTransaction()
                                .replace(R.id.container, MainActivity.MainFragment.newInstance())
                                .commit();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        accCreatorAlert.setTitle(R.string.set_accountCreate);
        accCreatorAlert.create();
        accCreatorAlert.show();
    }
}
