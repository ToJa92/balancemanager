package com.bm.balancemanager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class accountListObject {
    public accountListObject(){
        this.name = "ERROR";
        this.sum = "ERROR";
    }

    public accountListObject(JSONObject object){
        try {
            this.name = object.getString("description");
            this.sum = object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<accountListObject> fromJson(JSONArray jsonObjects) {
        ArrayList<accountListObject> mainobj = new ArrayList<accountListObject>();
        for(int i = 0; i < jsonObjects.length(); ++i){
            try {
                mainobj.add(new accountListObject(jsonObjects.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mainobj;
    }
    public String name;
    public String sum;
}
