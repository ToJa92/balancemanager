package com.bm.balancemanager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

public class HistoryFragment extends BaseFragment {
    private static final String TITLE = "History";

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString("title", TITLE);
        fragment.setArguments(args);
        return fragment;
    }

    public HistoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);

        ArrayList<HistoryListObject> history;
        HistoryListAdapter hAdapter = new HistoryListAdapter(getActivity());
        history = db_h.getHistory();
        hAdapter.addAll(history);
        ListView historyListView = (ListView) rootView.findViewById(R.id.historyList);
        historyListView.setAdapter(hAdapter);

        return rootView;
    }
}
