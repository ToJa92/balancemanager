package com.bm.balancemanager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.vessel.VesselAB;

public class newSettingsFragment extends BaseFragment {
    private static final String TITLE = "Settings";

    public static newSettingsFragment newInstance() {
        newSettingsFragment fragment = new newSettingsFragment();
        Bundle args = new Bundle();
        args.putString("title", TITLE);
        fragment.setArguments(args);
        return fragment;
    }

    public newSettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        TextView tvt = (TextView) rootView.findViewById(R.id.variation);

        tvt.setText(VesselAB.getValue("testAB", getString(R.string.placeholder)));

        Button dropDB = (Button) rootView.findViewById(R.id.dropdb);
        dropDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db_h.onUpgrade(db_h.getWritableDatabase(), 1, 1);
                preference_h.edit().putBoolean("SETUP_DONE", false).commit();
            }
        });

        return rootView;
    }
}

