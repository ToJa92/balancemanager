package com.bm.balancemanager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InitialSetup extends BaseFragment {

    private static final String TITLE = "Initial setup";

    public static InitialSetup newInstance() {
        InitialSetup fragment = new InitialSetup();
        Bundle args = new Bundle();
        args.putString("title", TITLE);
        fragment.setArguments(args);
        return fragment;
    }

    public InitialSetup() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_initial_setup, container, false);

        Button createAcc = (Button) rootView.findViewById(R.id.setupCreateAcc);
        createAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayNewAccCreator();
            }
        });

        return rootView;
    }
}
