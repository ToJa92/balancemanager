package com.bm.balancemanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

class DataBaseHandler extends SQLiteOpenHelper {
    private static final String DB_NAME = "balancemanager.sqlite";
    private static final int DB_VERSION = 2;
    private Context context = null;


    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }
    public DataBaseHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        // Store the context for later use
        this.context = context;
    }

    public long createAccount(String name, Float balance)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("balance", balance);

        return db.insert("accounts", null, values);
    }

    public long createCat(String name)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("name", name);
        return db.insert("categories", null, values);
    }

    public ArrayList<String> getAccounts()
    {
        ArrayList<String> accountNames = new ArrayList<String>();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM accounts", null);
        Log.w("DataBaseHandler", "selected from acc");
        if (cursor.moveToFirst()){
            Log.w("DataBaseHandler", "looping over cursor");
            do{
                String name = cursor.getString(cursor.getColumnIndex("name"));
                Float balance = cursor.getFloat(cursor.getColumnIndex("balance"));
                Log.w("DataBaseHandler", "adding: "+name);
                accountNames.add(name + ": " + balance.toString());
            }while(cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return accountNames;
    }

    public ArrayList<String> getCats()
    {
        ArrayList<String> catNames = new ArrayList<String>();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM categories", null);
        if (cursor.moveToFirst()){
            Log.w("DataBaseHandler", "looping over cat cursor");
            do{
                String name = cursor.getString(cursor.getColumnIndex("name"));
                Log.w("DataBaseHandler", "adding: "+name);
                catNames.add(name.toString());
            }while(cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return catNames;
    }

    public String getCat(int ID)
    {
        String catName = "Not Found";
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM categories", null);
        if (cursor.moveToFirst()){
            Log.w("DataBaseHandler", "looping over cat cursor");
            do{
                int name = cursor.getInt(cursor.getColumnIndex("id"));
                if (name == ID)
                {
                    catName = cursor.getString(cursor.getColumnIndex("name"));
                }
                Log.w("DataBaseHandler", "adding: "+name);
            }while(cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return catName;
    }

    public void updateBalance(int id, float change)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT balance FROM accounts WHERE id = "+id, null);
        cursor.moveToFirst();
        Float balance = cursor.getFloat(cursor.getColumnIndex("balance"));
        balance += change;
        ContentValues cv = new ContentValues();
        cv.put("balance", balance);
        db.update("accounts", cv, "id = "+id, null);
        cursor.close();
        db.close();
    }

    public void makeTransaction(String desc, Float amount, int catID, int acctID)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        long unixTime = System.currentTimeMillis() / 1000L;

        values.put("description", desc);
        values.put("sum", amount);
        values.put("timestamp", unixTime);
        values.put("catID", catID);
        values.put("accID", acctID);
        try
        {
            db.insertOrThrow("transactions", null, values);
        }
        catch (android.database.sqlite.SQLiteConstraintException e)
        {
            Log.e("DataBaseHandler", e.toString());
            db.close();
            return;
        }
        db.close();
        updateBalance(acctID, amount);
    }

    public ArrayList<accountListObject> getMainAccounts()
    {
        ArrayList<accountListObject> accounts = new ArrayList<accountListObject>();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();

        cursor = db.rawQuery("SELECT * FROM accounts", null);
        if (cursor.moveToLast())
        {
            Log.w("DataBaseHandler", "looping over acounts cursor");
            do
            {
                String name = cursor.getString(cursor.getColumnIndex("name"));
                Float sum = cursor.getFloat(cursor.getColumnIndex("balance"));
                Log.w("DataBaseHandler", "adding: "+name + " " + sum);

                accountListObject item = new accountListObject();

                item.name = name;

                item.sum = sum.toString();

                accounts.add(item);
            } while(cursor.moveToPrevious());
        }
        cursor.close();
        db.close();


        return accounts;
    }

    public ArrayList<HistoryListObject> getHistory()
    {
        ArrayList<HistoryListObject> history = new ArrayList<HistoryListObject>();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();

        cursor = db.rawQuery("SELECT * FROM transactions", null);
        if (cursor.moveToLast())
        {
            Log.w("DataBaseHandler", "looping over transaction cursor");
            do
            {
                String desc = cursor.getString(cursor.getColumnIndex("description"));
                Float sum = cursor.getFloat(cursor.getColumnIndex("sum"));
                Long time = cursor.getLong(cursor.getColumnIndex("timestamp"));
                int categoryID = cursor.getInt(cursor.getColumnIndex("catID"));

                String category = getCat(categoryID);

                Log.w("DataBaseHandler", "adding: "+desc + " " + sum + " " + time);

                HistoryListObject item = new HistoryListObject();

                item.description = desc;

                item.time = time;

                item.data = sum.toString();

                item.category = category;

                history.add(item);
            } while(cursor.moveToPrevious());
        }
        cursor.close();
        db.close();

        return history;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.w("DataBaseHandler", "Creating databases!");
        db.execSQL("CREATE TABLE accounts(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, balance FLOAT);");
        db.execSQL("CREATE TABLE categories(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);");
        db.execSQL("CREATE TABLE transactions(id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT, sum FLOAT, timestamp LONG, catID INTEGER, accID INTEGER, FOREIGN KEY (catID) REFERENCES categories (id), FOREIGN KEY (accID) REFERENCES accounts (id))");
        ContentValues values = new ContentValues();
        values.put("name", "Misc");
        db.insert("categories", null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
        Log.w("DataBaseHandler", "Emptying databases!");
        db.execSQL("DROP TABLE IF EXISTS transactions");
        db.execSQL("DROP TABLE IF EXISTS accounts");
        db.execSQL("DROP TABLE IF EXISTS categories");
        onCreate(db);
    }
}